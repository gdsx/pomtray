#include "Main.h"
#include <wx/msgdlg.h>
#include <wx/intl.h>
#include <wx/string.h>
#include <wx/sizer.h>
#include <wx/dcmemory.h>

#include "../res/pomtray.xpm"

#include "math.h"

extern PomtrayXFrame* gs_frame;


const long g_idButtonPomodoro   = wxNewId();
const long g_idButtonLongBreak  = wxNewId();
const long g_idButtonShortBreak = wxNewId();
const long g_idButtonStop       = wxNewId();
const long g_idButtonMinimize   = wxNewId();
const long g_idMenuQuit         = wxNewId();
const long g_idMenuAbout        = wxNewId();
const long g_idTimer            = wxNewId();

BEGIN_EVENT_TABLE(PomtrayXFrame,wxFrame)
    EVT_ICONIZE(OnIconize)
END_EVENT_TABLE()

PomtrayXFrame::PomtrayXFrame(wxWindow* parent,wxWindowID id)
{
    m_pStopWatch = nullptr;
    m_nInitialTime = 0;
    m_bDone = false;

    wxMenuItem* MenuItem2;
    wxMenuItem* MenuItem1;
    wxMenu* Menu1;
    wxMenuBar* MenuBar1;
    wxMenu* Menu2;

    Create(parent, id, wxEmptyString, wxDefaultPosition, wxSize(200, 250), wxDEFAULT_FRAME_STYLE, _T("id"));
    m_pClock   = new wxStaticText(this, wxID_ANY, wxT("Text"));
    m_pButtonPomodoro = new wxButton(this, g_idButtonPomodoro, _("[25:00]  Pomodoro"));
    m_pButtonLongBreak = new wxButton(this, g_idButtonLongBreak, _("[10:00]  Long break"));
    m_pButtonShortBreak = new wxButton(this, g_idButtonShortBreak, _("[05:00]  Short break"));
    m_pButtonStop  = new wxButton(this, g_idButtonStop, _("[--:--]  Stop"));
    m_pButtonMinimize = new wxButton(this, g_idButtonMinimize, _("Minimize"));

    wxFont font = m_pClock->GetFont();
    font.SetPointSize(40);
    font.SetWeight(wxFONTWEIGHT_BOLD);
    m_pClock->SetFont(font);

    wxSizer * const sizerTop = new wxBoxSizer(wxVERTICAL);
    wxSizerFlags flags;
    flags.Border(wxALL, 1).Expand();
    sizerTop->Add(m_pClock, flags);
    wxSizer * const sizerCols = new wxBoxSizer(wxHORIZONTAL);
        wxSizer * const sizerBtn = new wxBoxSizer(wxVERTICAL);
            sizerBtn->Add(m_pButtonPomodoro, flags);
            sizerBtn->Add(m_pButtonLongBreak, flags);
            sizerBtn->Add(m_pButtonShortBreak, flags);
        sizerCols->Add(sizerBtn, flags);
        sizerCols->Add(m_pButtonMinimize, flags);
    sizerTop->Add(sizerCols, flags);
    sizerTop->Add(m_pButtonStop, flags);
    SetSizer(sizerTop);
    Centre();

    MenuBar1 = new wxMenuBar();
    Menu1 = new wxMenu();
    MenuItem1 = new wxMenuItem(Menu1, g_idMenuQuit, _("Quit\tAlt-F4"), _("Quit the application"), wxITEM_NORMAL);
    Menu1->Append(MenuItem1);
    MenuBar1->Append(Menu1, _("&File"));
    Menu2 = new wxMenu();
    MenuItem2 = new wxMenuItem(Menu2, g_idMenuAbout, _("About\tF1"), _("Show info about this application"), wxITEM_NORMAL);
    Menu2->Append(MenuItem2);
    MenuBar1->Append(Menu2, _("Help"));
    SetMenuBar(MenuBar1);

    Connect(g_idButtonPomodoro,wxEVT_COMMAND_BUTTON_CLICKED,(wxObjectEventFunction)&PomtrayXFrame::OnButtonPomodoroClick);
    Connect(g_idButtonLongBreak,wxEVT_COMMAND_BUTTON_CLICKED,(wxObjectEventFunction)&PomtrayXFrame::OnButtonLongBreakClick);
    Connect(g_idButtonShortBreak,wxEVT_COMMAND_BUTTON_CLICKED,(wxObjectEventFunction)&PomtrayXFrame::OnButtonShortBreakClick);
    Connect(g_idButtonStop,wxEVT_COMMAND_BUTTON_CLICKED,(wxObjectEventFunction)&PomtrayXFrame::OnButtonStopClick);
    Connect(g_idButtonMinimize,wxEVT_COMMAND_BUTTON_CLICKED,(wxObjectEventFunction)&PomtrayXFrame::OnButtonMinimizeClick);
    Connect(g_idMenuQuit,wxEVT_COMMAND_MENU_SELECTED,(wxObjectEventFunction)&PomtrayXFrame::OnQuit);
    Connect(g_idMenuAbout,wxEVT_COMMAND_MENU_SELECTED,(wxObjectEventFunction)&PomtrayXFrame::OnAbout);

    wxAcceleratorEntry entries[1];
    entries[0].Set(wxACCEL_NORMAL, WXK_ESCAPE, g_idButtonMinimize);
    wxAcceleratorTable accel(1, entries);
    SetAcceleratorTable(accel);


    m_taskBarIcon = new MyTaskBarIcon();
    //m_taskBarIcon->SetIcon(wxIcon(tray_xpm), wxT("ready"));
    UpdateClock();

    m_pTimer = new wxTimer(this, g_idTimer);
    Connect(g_idTimer, wxEVT_TIMER, (wxObjectEventFunction)&PomtrayXFrame::OnProgressTimer);
    m_pTimer->Start(500);
}

PomtrayXFrame::~PomtrayXFrame()
{
    delete m_pStopWatch;
    delete m_pTimer;
    delete m_taskBarIcon;
}

void PomtrayXFrame::UpdateClock()
{
    if (!m_pStopWatch) {
        m_pClock->SetLabel(wxT("--:--"));
        pomtray_xpm[2] = "X c Gray";

        wxIcon pomtray_icon(pomtray_xpm);
        m_taskBarIcon->SetIcon(pomtray_icon, wxT("ready"));
        m_bDone = false;
        return;
    }
    const long nRemains = m_nInitialTime - (int)round(m_pStopWatch->Time() / 1000);
    //if (nRemains < 0) {
    //    m_pClock->SetLabel(wxT("00:00"));
    //    m_taskBarIcon->SetIcon(wxIcon(finished_xpm), wxT("wxTaskBarIcon"));
    //    return;
    //}

    const long nRemains0 = std::max(0L, nRemains);
    const long nMin = nRemains0 / 60;
    const long nSec = nRemains0 % 60;
    wxString sClock = wxString::Format(wxT("%.2d:%.2d"), nMin, nSec);
    m_pClock->SetLabel(sClock);



    if (nRemains0 > 0) {
        const double k = (double) nRemains0 / m_nInitialTime;
        if (k < 0.10) {
            pomtray_xpm[2] = "X c Orange";
            pomtray_xpm[3] = ", c Orange";
            pomtray_xpm[4] = "o c Orange";
        }
        else if (k < 0.50) {
            pomtray_xpm[2] = "X c Yellow";
            pomtray_xpm[3] = ", c Yellow";
            pomtray_xpm[4] = "o c Yellow";
        }
        else {
            pomtray_xpm[2] = "X c #00DD00"; //green
            pomtray_xpm[3] = ", c #00DD00";
            pomtray_xpm[4] = "o c #00DD00";
        }
            /*
        //int r = std::max(0,   (int) (2*255 * (1 - k)));
        //int g = std::min(255, (int) (2*255 * (k)));
        int r = (int) (255 * (1-k));
        int g = 100 + (int) (155 * (1-k));
        int b = 0;
        wxColour color(r, g, b);
        wxString s = "X c ";
        s += color.GetAsString(wxC2S_HTML_SYNTAX);
        tray_xpm[2] = s.c_str();
        */

        wxString sText = wxString::Format("%d", nMin);
        if (nMin < 10) {
            sText = " " + sText;
        }
        wxIcon tray_icon;
        wxBitmap bmp(pomtray_xpm, wxBITMAP_TYPE_XPM_DATA);
        wxMemoryDC dc;
        dc.SelectObject(bmp);
        wxFont font = dc.GetFont();
        font.SetPointSize(15);
        dc.SetFont(font);
        dc.DrawText(sText, 5, 5);
        dc.SelectObject(wxNullBitmap);
        tray_icon.CopyFromBitmap(bmp);
        m_taskBarIcon->SetIcon(tray_icon, sClock);
    }
    else {
        m_bDone = true;
        if (nRemains%2) {
            pomtray_xpm[2] = "X c Red";
            pomtray_xpm[3] = ", c Black";
            pomtray_xpm[4] = "o c #C00000";
        }
        else {
            pomtray_xpm[2] = "X c Yellow";
            pomtray_xpm[3] = ", c Black";
            pomtray_xpm[4] = "o c #C00000";
        }
        wxIcon tray_icon(pomtray_xpm);
        m_taskBarIcon->SetIcon(tray_icon, sClock);
    }
}

void PomtrayXFrame::OnIconize(wxIconizeEvent& event)
{
    Show(!event.IsIconized());
}

void PomtrayXFrame::OnQuit(wxCommandEvent& event)
{
    Close();
}

void PomtrayXFrame::OnAbout(wxCommandEvent& event)
{
    wxMessageBox(_("Pomtray 0.1"), _("Pomtray"));
}

void PomtrayXFrame::OnButtonPomodoroClick(wxCommandEvent& event)
{
    m_nInitialTime = 25*60;
    #ifdef _DEBUG
        m_nInitialTime /= 60;
    #endif

    delete m_pStopWatch;
    m_pStopWatch = new wxStopWatch();
    m_bDone = false;
    UpdateClock();
}

void PomtrayXFrame::OnButtonLongBreakClick(wxCommandEvent& event)
{
    m_nInitialTime = 10*60;
    #ifdef _DEBUG
        m_nInitialTime /= 60;
    #endif

    delete m_pStopWatch;
    m_pStopWatch = new wxStopWatch();
    m_bDone = false;
    UpdateClock();
}

void PomtrayXFrame::OnButtonShortBreakClick(wxCommandEvent& event)
{
    m_nInitialTime = 5*60;
    #ifdef _DEBUG
        m_nInitialTime /= 60;
    #endif

    delete m_pStopWatch;
    m_pStopWatch = new wxStopWatch();
    m_bDone = false;
    UpdateClock();
}

void PomtrayXFrame::OnButtonStopClick(wxCommandEvent& event)
{
    m_nInitialTime = 0;
    delete m_pStopWatch;
    m_pStopWatch = nullptr;
    m_bDone = false;
    UpdateClock();
}

void PomtrayXFrame::OnButtonMinimizeClick(wxCommandEvent& event)
{
    gs_frame->Hide();
}

void PomtrayXFrame::OnProgressTimer(wxTimerEvent& event)
{
    UpdateClock();
}

bool PomtrayXFrame::ResetIfDone()
{
    if (m_bDone) {
        m_bDone = false;
        delete m_pStopWatch;
        m_pStopWatch = nullptr;
        UpdateClock();
        return true;
    }
    return false;
}




// ----------------------------------------------------------------------------
// MyTaskBarIcon implementation
// ----------------------------------------------------------------------------

enum
{
    PU_RESTORE = 10001,
    PU_POMODORO,
    PU_LONGBREAK,
    PU_SHORTBREAK,
    PU_STOP,
    PU_EXIT,
};


wxBEGIN_EVENT_TABLE(MyTaskBarIcon, wxTaskBarIcon)
    //EVT_TASKBAR_LEFT_DCLICK  (MyTaskBarIcon::OnLeftButtonClick)
    //EVT_TASKBAR_MOVE(MyTaskBarIcon::OnLeftButtonClick)
    EVT_TASKBAR_LEFT_DOWN(MyTaskBarIcon::OnLeftButtonClick)
    EVT_MENU(PU_RESTORE,    MyTaskBarIcon::OnMenuRestore)
    EVT_MENU(PU_POMODORO,   MyTaskBarIcon::OnMenuPomodoro)
    EVT_MENU(PU_LONGBREAK,  MyTaskBarIcon::OnMenuPomodoro)
    EVT_MENU(PU_SHORTBREAK, MyTaskBarIcon::OnMenuPomodoro)
    EVT_MENU(PU_STOP,       MyTaskBarIcon::OnMenuPomodoro)
    EVT_MENU(PU_EXIT,       MyTaskBarIcon::OnMenuExit)
wxEND_EVENT_TABLE()

wxMenu *MyTaskBarIcon::CreatePopupMenu()
{
    wxMenu *menu = new wxMenu;
    menu->Append(PU_RESTORE,    wxT("&Restore main window"));
    menu->AppendSeparator();
    menu->Append(PU_POMODORO,   wxT("[25:00]  &Pomodoro"));
    menu->Append(PU_LONGBREAK,  wxT("[10:00]  &Long break"));
    menu->Append(PU_SHORTBREAK, wxT("[05:00]  &Short break"));
    menu->Append(PU_STOP,       wxT("[--:--]  S&top"));
    //menu->AppendSeparator();
    //menu->AppendCheckItem(PU_CHECKMARK, wxT("Test &check mark"));
    //wxMenu *submenu = new wxMenu;
    //submenu->Append(PU_SUB1, wxT("One submenu"));
    //submenu->Append(PU_SUB2, wxT("Another submenu"));
    //menu->Append(PU_SUBMAIN, wxT("Submenu"), submenu);
    menu->AppendSeparator();
    menu->Append(PU_EXIT,    wxT("E&xit"));
    return menu;
}

void MyTaskBarIcon::OnLeftButtonClick(wxTaskBarIconEvent&) {
    if (gs_frame->ResetIfDone()) {
        return;
    }
    if (gs_frame->IsVisible()) {
        gs_frame->Show(false);
        gs_frame->Iconize(true);
    }
    else {
        gs_frame->Iconize(false);
        gs_frame->SetFocus();
        gs_frame->Raise();
        gs_frame->Show(true);
    }
}

void MyTaskBarIcon::OnMenuRestore(wxCommandEvent& ) {
    gs_frame->Show(true);
}

void MyTaskBarIcon::OnMenuPomodoro(wxCommandEvent &event) {
    wxCommandEvent event2;
    switch (event.GetId()) {
    case PU_POMODORO:
        gs_frame->OnButtonPomodoroClick(event2);
        break;
    case PU_LONGBREAK:
        gs_frame->OnButtonLongBreakClick(event2);
        break;
    case PU_SHORTBREAK:
        gs_frame->OnButtonShortBreakClick(event2);
        break;
    case PU_STOP:
        gs_frame->OnButtonStopClick(event2);
        break;
    }
}

void MyTaskBarIcon::OnMenuExit(wxCommandEvent& ) {
    gs_frame->Close(true);
}

//static bool check = true;
//void MyTaskBarIcon::OnMenuCheckmark(wxCommandEvent& ) {
//    check = !check;
//}
//void MyTaskBarIcon::OnMenuUICheckmark(wxUpdateUIEvent &event) {
//    event.Check(check);
//}
//
//void MyTaskBarIcon::OnMenuSub(wxCommandEvent&) {
//    wxMessageBox(wxT("You clicked on a submenu!"));
//}

