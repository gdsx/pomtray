#ifndef FSNOTESXMAIN_H
#define FSNOTESXMAIN_H

#include <wx/menu.h>
#include <wx/button.h>
#include <wx/frame.h>
#include <wx/statusbr.h>
#include "wx/taskbar.h"
#include <wx/timer.h>
#include <wx/stattext.h>

class MyTaskBarIcon : public wxTaskBarIcon
{
public:
    MyTaskBarIcon() { }

public:
    void OnLeftButtonClick(wxTaskBarIconEvent&);
    void OnMenuRestore(wxCommandEvent&);
    void OnMenuPomodoro(wxCommandEvent&);
    void OnMenuExit(wxCommandEvent&);
    //void OnMenuCheckmark(wxCommandEvent&);
    //void OnMenuUICheckmark(wxUpdateUIEvent&);
    //void OnMenuSub(wxCommandEvent&);
    virtual wxMenu *CreatePopupMenu();
    wxDECLARE_EVENT_TABLE();
};

class PomtrayXFrame: public wxFrame
{
    public:

        PomtrayXFrame(wxWindow* parent,wxWindowID id = -1);
        virtual ~PomtrayXFrame();

    public:
        bool ResetIfDone();

    private:
        void UpdateClock();

    //private:
    public:
        void OnQuit(wxCommandEvent& event);
        void OnAbout(wxCommandEvent& event);
        void OnButtonPomodoroClick(wxCommandEvent& event);
        void OnButtonLongBreakClick(wxCommandEvent& event);
        void OnButtonShortBreakClick(wxCommandEvent& event);
        void OnButtonStopClick(wxCommandEvent& event);
        void OnButtonMinimizeClick(wxCommandEvent& event);
        void OnIconize(wxIconizeEvent& event);
        void OnProgressTimer(wxTimerEvent& event);

        wxStaticText *m_pClock;
        wxButton     *m_pButtonPomodoro;
        wxButton     *m_pButtonLongBreak;
        wxButton     *m_pButtonShortBreak;
        wxButton     *m_pButtonStop;
        wxButton     *m_pButtonMinimize;

        DECLARE_EVENT_TABLE()

        MyTaskBarIcon   *m_taskBarIcon;

        wxTimer *m_pTimer;
        wxStopWatch *m_pStopWatch;
        long m_nInitialTime;
        bool m_bDone;

};

#endif // FSNOTESXMAIN_H
