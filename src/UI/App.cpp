#include "App.h"
#include "Main.h"
#include <wx/msgdlg.h>

IMPLEMENT_APP(PomtrayXApp);

PomtrayXFrame* gs_frame = nullptr;

bool PomtrayXApp::OnInit()
{
    if (!wxApp::OnInit()) {
        return false;
    }
    if (!wxTaskBarIcon::IsAvailable()) {
        wxMessageBox("There appears to be no system tray support in your current environment. This sample may not behave as expected.",
                        "Warning", wxOK | wxICON_EXCLAMATION);
    }
    gs_frame = new PomtrayXFrame(0);
    gs_frame->Show();
    SetTopWindow(gs_frame);
    return true;
}
