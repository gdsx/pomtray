#ifndef FSNOTESXAPP_H
#define FSNOTESXAPP_H

#include <wx/app.h>

class PomtrayXApp : public wxApp
{public:
    virtual bool OnInit();
};

#endif // FSNOTESXAPP_H
